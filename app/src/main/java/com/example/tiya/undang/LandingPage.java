package com.example.tiya.undang;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

public class LandingPage extends AppCompatActivity {

    GridView gridView;

    int[] fruitImages = {R.drawable.satu, R.drawable.dua, R.drawable.tiga, R.drawable.empat, R.drawable.lima,
            R.drawable.enam, R.drawable.tujuh, R.drawable.delapan, R.drawable.sembilan, R.drawable.sepuluh,
            R.drawable.sebelas, R.drawable.duabelas, R.drawable.tigabelas, R.drawable.empatbelas, R.drawable.limabelas,
            R.drawable.enambelas, R.drawable.tujuhbelas, R.drawable.delapanbelas, R.drawable.sembilanbelas, R.drawable.duapuluh,
            R.drawable.duasatu, R.drawable.duadua,
//            R.drawable.dualima,R.drawable.dualapan,
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landingpage);

        //finding listview
        gridView = findViewById(R.id.gridview);

        CustomAdapter customAdapter = new CustomAdapter();
        gridView.setAdapter(customAdapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                Toast.makeText(getApplicationContext(),Toast.LENGTH_LONG).show();
                Intent in = new Intent(LandingPage.this, HalamanEdit.class);
                in.putExtra("image", fruitImages[i]);
                startActivity(in);
            }
        });


    }

    private class CustomAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return fruitImages.length;
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            View view1 = getLayoutInflater().inflate(R.layout.activity_rowdata, null);
            //getting view in row_data
            ImageView image = view1.findViewById(R.id.images);

            image.setImageResource(fruitImages[i]);
            return view1;


        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.credit:
                Intent credit = new Intent(this, CreditActivity.class);
                startActivity(credit);
                break;
            case android.R.id.home:
                onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
