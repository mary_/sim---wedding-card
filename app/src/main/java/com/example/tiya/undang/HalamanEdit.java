package com.example.tiya.undang;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.internal.NavigationMenu;
import android.support.design.internal.NavigationMenuItemView;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.rtugeek.android.colorseekbar.ColorSeekBar;

import java.io.File;
import java.io.FileOutputStream;

public class HalamanEdit extends AppCompatActivity {
    RelativeLayout idForSaveView;
    ImageView imageView;
    private EditText mnamawanita;
    private EditText mnamapria;
    private EditText mand;
    private EditText mtanggal;
    private EditText malamat;
    ImageButton btn_undo;
    LinearLayout bottomSheet;
    LinearLayout bottomSheet2;
    LinearLayout bottomSheet3;
    Button btn_font;
    Button kembali;
    Button btn_fontcolor;
    Button kembali2;
    private SharedPreferences prefs;
    private String prefName = "MyPref";
    private EditText editText;
    private SeekBar seekBar;
    private Button btn;
    private static final String FONT_SIZE_KEY = "fontsize";
    private static final String TEXT_VALUE_KEY = "textvalue";
//
//    int currentState = -1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_halamanedit);

        //WARNA
        mnamawanita=(EditText) findViewById(R.id.namawanita);
        mnamapria=(EditText) findViewById(R.id.namapria);
        mand=(EditText) findViewById(R.id.and);
        mtanggal=(EditText) findViewById(R.id.tanggal);
        malamat=(EditText) findViewById(R.id.alamat);
        ColorSeekBar colorSeekBar = findViewById(R.id.color_seek_bar);
        colorSeekBar.setOnColorChangeListener(new ColorSeekBar.OnColorChangeListener() {
            @Override
            public void onColorChangeListener(int i, int i1, int i2) {
                mnamapria.setTextColor(i2);
                mnamawanita.setTextColor(i2);
                mand.setTextColor(i2);
                mtanggal.setTextColor(i2);
                malamat.setTextColor(i2);
            }
        });

        //TAMPIL FONT
//        mnamawanita=(EditText) findViewById(R.id.namawanita);
//        mnamapria=(EditText) findViewById(R.id.namapria);
//        mand=(EditText) findViewById(R.id.and);
//        mtanggal=(EditText) findViewById(R.id.tanggal);
//        malamat=(EditText) findViewById(R.id.alamat);


        ListView listView=(ListView)findViewById(R.id.mainListView);

        final AdapterListView adapterKu=new AdapterListView(this);

        listView.setAdapter(adapterKu);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> p1, View p2, int posisi, long p3)
            {
// TODO: Implement this method
                mnamawanita.setTypeface((Typeface)adapterKu.getItem(posisi));
                mnamapria.setTypeface((Typeface)adapterKu.getItem(posisi));
                mand.setTypeface((Typeface)adapterKu.getItem(posisi));
                mtanggal.setTypeface((Typeface)adapterKu.getItem(posisi));
                malamat.setTypeface((Typeface)adapterKu.getItem(posisi));
            }
        });




//        VISIBLE GONE FONT
        //Sheet ubah font
        bottomSheet = (LinearLayout) findViewById(R.id.llBottomSheet);
        //Sheet ubah color
        bottomSheet2 = (LinearLayout) findViewById(R.id.llBottomSheet2);


        btn_font = (Button) findViewById(R.id.btn_font);
        kembali = (Button) findViewById(R.id.kembali);
        bottomSheet2.setVisibility(LinearLayout.GONE);
//        bottomSheet3.setVisibility(LinearLayout.GONE);
        bottomSheet.setVisibility(LinearLayout.GONE);

        btn_font.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                bottomSheet.setVisibility(LinearLayout.VISIBLE);
                bottomSheet2.setVisibility(LinearLayout.GONE);


            }
        });
        kembali.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                bottomSheet2.setVisibility(LinearLayout.GONE);
                bottomSheet.setVisibility(LinearLayout.GONE);
//                bottomSheet3.setVisibility(LinearLayout.GONE);

            }
        });

        //        VISIBLE GONE FONT SIZE

        btn_fontcolor = (Button) findViewById(R.id.btn_fontcolor);
        kembali2 = (Button) findViewById(R.id.kembali2);
        bottomSheet2.setVisibility(LinearLayout.GONE);
//        bottomSheet3.setVisibility(LinearLayout.GONE);
        bottomSheet.setVisibility(LinearLayout.GONE);
        btn_fontcolor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                bottomSheet2.setVisibility(LinearLayout.VISIBLE);
                bottomSheet.setVisibility(LinearLayout.GONE);


            }
        });
        kembali2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                bottomSheet2.setVisibility(LinearLayout.GONE);
                bottomSheet.setVisibility(LinearLayout.GONE);
//                bottomSheet3.setVisibility(LinearLayout.GONE);

            }
        });


        if (!isStoragePermissionGranted()) {
            //Toast.makeText(this, "Maaf, anda harus menyetujui permission storage", Toast.LENGTH_SHORT).show();
        } else {
            //Toast.makeText(this, "Pastikan penyimpanan mencukupi", Toast.LENGTH_SHORT).show();
            mpInit();
        }
        idForSaveView = (RelativeLayout) findViewById(R.id.idForSaveView);

        imageView = findViewById(R.id.imageView);
        Intent intent = getIntent();
        int receivedImage = intent.getIntExtra("image", 0);

        imageView.setImageResource(receivedImage);
        //enable back Button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


    }


    public void mpInit() {
        // All player buttons
        //btn_Save = (Button) findViewById(R.id.btn_save);
        Splash splash = new Splash();

    }


    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(HalamanEdit.this, "Permission granted." + permissions[0] + "was" + grantResults[0], Toast.LENGTH_SHORT).show();
            //resume tasks needing this permission
            if (permissions[0] == android.Manifest.permission.WRITE_EXTERNAL_STORAGE) {
                mpInit();
            }
        }
    }

    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                //Toast.makeText(HalamanEdit.this, "Permission granted.", Toast.LENGTH_SHORT).show();
                return true;
            } else {
                Toast.makeText(HalamanEdit.this, "", Toast.LENGTH_SHORT).show();
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else {
            //Toast.makeText(HalamanEdit.this, "Permission granted.", Toast.LENGTH_SHORT).show();
            return true;
        }
    }

    private Bitmap getBitmapFromView(View view) {
        Bitmap returnedBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable != null) {
            //has background drawable, then draw it on the canvas
            bgDrawable.draw(canvas);
        } else {
            //does not have background drawable, then draw white background on the canvas
            canvas.drawColor(Color.WHITE);
        }
        view.draw(canvas);
        return returnedBitmap;
    }


    public void OnClickShare(View view) {

        Bitmap bitmap = getBitmapFromView(idForSaveView);
        try {
            File file = new File(this.getExternalCacheDir(), "logicchip.png");
            FileOutputStream fOut = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut);
            fOut.flush();
            fOut.close();
            file.setReadable(true, false);
            final Intent intent = new Intent(android.content.Intent.ACTION_SEND);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
            intent.setType("image/png");
            startActivity(Intent.createChooser(intent, "Share image via"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void OnClickSave(View view) {
        String root = Environment.getExternalStorageDirectory().getAbsolutePath();
        File myDir = new File(root + "/WeddingCard");
        myDir.mkdirs();

        try {
            String fname = "Image-" + System.currentTimeMillis() + ".jpg";
            File file = new File(myDir, fname);
            if (file.exists()) file.delete();
            FileOutputStream fOut = new FileOutputStream(file);
            Bitmap bitmap = getBitmapFromView(idForSaveView);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut);
            Toast.makeText(this, "Save image success", Toast.LENGTH_SHORT).show();
            fOut.flush();
            fOut.close();
            file.setReadable(true, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu2, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.credit:
                Intent credit = new Intent(this, CreditActivity.class);
                startActivity(credit);
                break;
            case android.R.id.home:
                onBackPressed();
                break;
//            case R.id.save:
//                String root = Environment.getExternalStorageDirectory().getAbsolutePath();
//                File myDir = new File(root + "/WeddingCard");
//                myDir.mkdirs();
//
//                try {
//                    String fname = "Image-" + System.currentTimeMillis() + ".jpg";
//                    File file = new File(myDir, fname);
//                    if (file.exists()) file.delete();
//                    FileOutputStream fOut = new FileOutputStream(file);
//                    Bitmap bitmap = getBitmapFromView(idForSaveView);
//                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut);
//                    Toast.makeText(this, "Save image success", Toast.LENGTH_SHORT).show();
//                    fOut.flush();
//                    fOut.close();
//                    file.setReadable(true, false);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                break;
//            case R.id.undo:
//                try {
//                    mnamawanita.setText("");
//                    mnamapria.setText("");
//                    mand.setText("");
//                    mtanggal.setText("");
//                    malamat.setText("");
//                }catch (Exception e) {
//                    e.printStackTrace();
//                }
//                break;
        }

        return super.onOptionsItemSelected(item);


    }



public void OnClickUndo(View view) {
    try {
        mnamawanita.setText("");
        mnamapria.setText("");
        mand.setText("");
        mtanggal.setText("");
        malamat.setText("");
        }catch (Exception e) {
        e.printStackTrace();
    }
}
    }