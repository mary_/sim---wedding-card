package com.example.tiya.undang;

import android.view.*;
import android.widget.*;
import java.util.*;
import android.content.*;
import android.content.res.*;
import java.io.*;
import android.graphics.*;

public class AdapterListView extends BaseAdapter
{
    private Context context;
    private String[] listFontDiAsset;
    private AssetManager assetManager;


    public AdapterListView(Context context){
        this.context=context;
        assetManager=context.getAssets();
        try
        {
            listFontDiAsset = assetManager.list("fonts");
        }
        catch (IOException e)
        {
            listFontDiAsset=new String[0];
        }
    }

    @Override
    public int getCount()
    {
        // TODO: Implement this method
        return listFontDiAsset.length;
    }

    @Override
    public Object getItem(int p1)
    {
        // TODO: Implement this method
        return Typeface.createFromAsset(assetManager,"fonts/"+listFontDiAsset[p1]);
    }

    @Override
    public long getItemId(int p1)
    {
        // TODO: Implement this method
        return p1;
    }

    @Override
    public View getView(int posisi, View view, ViewGroup p3)
    {
        // TODO: Implement this method
        TextView mnamapria=new TextView(context);
        mnamapria.setPadding(20,20,20,20);
        mnamapria.setTextSize(20f);
        mnamapria.setText(listFontDiAsset[posisi]);

        mnamapria.setTypeface(Typeface.createFromAsset(assetManager,"fonts/"+listFontDiAsset[posisi]));


        return mnamapria;


    }

}
