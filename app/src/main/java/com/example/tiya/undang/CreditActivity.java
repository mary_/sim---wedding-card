package com.example.tiya.undang;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.util.Linkify;
import android.widget.TextView;

public class CreditActivity extends AppCompatActivity {

    TextView freepik, flaticon, dafont;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_credit);

        freepik = (TextView) findViewById(R.id.freepik);
        freepik.setText("www.freepik.com");
        Linkify.addLinks(freepik, Linkify.ALL);

        flaticon = (TextView) findViewById(R.id.flaticon);
        flaticon.setText("www.flaticon.com");
        Linkify.addLinks(flaticon, Linkify.ALL);

        dafont = (TextView) findViewById(R.id.dafont);
        dafont.setText("www.dafont.com");
        Linkify.addLinks(dafont, Linkify.ALL);
    }
}
